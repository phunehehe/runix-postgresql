{ home, pkgs, ... }:
let

  data-directory = "${home}/data";

  hba-file = pkgs.writeText "pg_hba.conf" ''
    local all postgres              ident
    local all all                   ident
    host  all all      127.0.0.1/32 md5
    host  all all      ::1/128      md5
  '';

  ident-file = pkgs.writeText "pg_ident.conf" ''
  '';

  config-file = pkgs.writeText "postgresql.conf" ''
    data_directory = '${data-directory}'
    hba_file = '${hba-file}'
    ident_file = '${ident-file}'
  '';

  processes = {

    # https://www.postgresql.org/docs/current/static/server-shutdown.html
    postgresql = ''
      ${pkgs.runit}/bin/chpst -u postgres \
        ${pkgs.translateSignal} TERM INT \
          ${pkgs.postgresql}/bin/postgres -c config_file=${config-file}
    '';

    vacuumdb = pkgs.writeBash "postgresql-vacuumdb" ''
      ${pkgs.runit}/bin/chpst -u postgres \
        ${pkgs.postgresql}/bin/vacuumdb --all
      exec ${pkgs.coreutils}/bin/sleep $((60 * 60 * 24))
    '';
  };

in pkgs.runsvdir home processes
